package com.floorwatch.test.rest;

import com.floorwatch.common.pojos.SimpleFlare;
import com.floorwatch.common.pojos.SimpleUser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class TestFlareResourceInitialManager {

    public static void main(String[] args) {
        try {
            SimpleFlare flare = new SimpleFlare();
            flare.setId(22);
            SimpleUser managerUser = new SimpleUser();
            managerUser.setId(5);
            flare.setManagerUser(managerUser);
            flare.setManagerText("I can't find you.");
            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getFeatures().put(
                    JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            Client client = Client.create(clientConfig);
            // Localhost
            WebResource webResource = client.resource("http://localhost:8080/rest/flare");
            // AWS
            //WebResource webResource = client.resource("http://52.88.254.252/rest/flare");
            ClientResponse response = webResource
                    .type("application/json").put(ClientResponse.class, flare);
            if (response.getStatus() != 204) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
