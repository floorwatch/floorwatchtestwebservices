package com.floorwatch.test.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.codec.binary.Base64;

public class TestBase64EncryptJpeg {

    public static void main(String[] args) {
        try {
            InputStream inputStream = new FileInputStream(new File("src/main/resources/macys.jpg"));
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = out.toByteArray();
            String encodedString = Base64.encodeBase64URLSafeString(bytes);
            System.out.println("Dicks: " + encodedString);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
