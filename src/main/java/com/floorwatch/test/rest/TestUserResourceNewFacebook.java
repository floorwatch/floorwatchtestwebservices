package com.floorwatch.test.rest;

import com.floorwatch.common.pojos.SimpleNetwork;
import com.floorwatch.common.pojos.SimpleUser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.commons.codec.binary.Base64;

public class TestUserResourceNewFacebook {

    public static void main(String[] args) {
        try {
            SimpleUser user = new SimpleUser();
            user.setUserName("rocky.davis.5602");
            user.setFirstName("Rocky");
            user.setLastName("Davis");
            user.setEmailAddress("ddavis1933@gmail.com");
            user.setNetworks(new ArrayList());
            SimpleNetwork network = new SimpleNetwork();
            network.setDescription(SimpleNetwork.Network.Facebook.toString());
            network.setUid("112855475736382");
            InputStream inputStream = new FileInputStream(new File("src/main/resources/rocky.jpg"));
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = out.toByteArray();
            String encodedString = Base64.encodeBase64URLSafeString(bytes);  
            network.setProfilePicture(encodedString);
            user.getNetworks().add(network);
            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getFeatures().put(
                    JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            Client client = Client.create(clientConfig);
            // Localhost
            WebResource webResource = client.resource("http://localhost:8080/rest/networkuser");
            // AWS
            //WebResource webResource = client.resource("http://52.88.254.252/rest/networkuser");
            ClientResponse response = webResource
                    .type("application/json").post(ClientResponse.class, user);
            if (response.getStatus() != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
            String output = response.getEntity(String.class);
            System.out.println("Server response .... \n");
            System.out.println(output);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
