package com.floorwatch.test.rest;

import com.floorwatch.common.pojos.SimpleFlare;
import com.floorwatch.common.pojos.SimpleStore;
import com.floorwatch.common.pojos.SimpleUser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import java.util.Date;

public class TestCompleteFlareEntry {

    public static void main(String[] args) {
        try {
            // POST
            SimpleFlare flare = new SimpleFlare();
            SimpleUser customerUser = new SimpleUser();
            customerUser.setId(7);
            flare.setCustomerUser(customerUser);
            SimpleStore store = new SimpleStore();
            store.setId(1);
            flare.setStore(store);
            SimpleUser managerUser = new SimpleUser();
            managerUser.setId(5);
            flare.setManagerUser(managerUser);
            flare.setCustomerText("I need help in Shoes.");
            flare.setManagerText("I can't find you.");
            flare.setCustomerFollowupText("I'm next to the Asics.");
            flare.setManagerFollowupText("OK, I see you.");
            flare.setResolved(true);
            flare.setResolvedAt(new Date().getTime());
            flare.setResolvedStars(4);
            flare.setResolvedText("The service was great.");
            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getFeatures().put(
                    JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            Client client = Client.create(clientConfig);
            // Localhost
            WebResource webResource = client.resource("http://localhost:8080/rest/flare");
                // AWS
            //WebResource webResource = client.resource("http://52.88.254.252/rest/flare");
            ClientResponse response = webResource
                    .type("application/json").post(ClientResponse.class, flare);
            if (response.getStatus() != 201) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
            String output = response.getEntity(String.class);
            System.out.println("Server response .... \n");
            System.out.println(output);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
