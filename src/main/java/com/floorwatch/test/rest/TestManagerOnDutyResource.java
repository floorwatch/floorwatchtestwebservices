package com.floorwatch.test.rest;

import com.floorwatch.common.pojos.SimpleStore;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.common.pojos.SimpleUserStore;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class TestManagerOnDutyResource {

    public static void main(String[] args) {
        try {
            SimpleUserStore userStore = new SimpleUserStore();
            SimpleUser user = new SimpleUser();
            SimpleStore store = new SimpleStore();
            user.setId(5);
            userStore.setUser(user);
            store.setId(1);
            userStore.setStore(store);
            userStore.setOnDuty(true);
            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getFeatures().put(
                    JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            Client client = Client.create(clientConfig);
            // Localhost
            WebResource webResource = client.resource("http://localhost:8080/rest/manager");
            // AWS
            //WebResource webResource = client.resource("http://52.88.254.252/rest/manager");            
            ClientResponse response = webResource
                    .type("application/json").put(ClientResponse.class, userStore);
            if (response.getStatus() != 204) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
