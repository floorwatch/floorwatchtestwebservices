package com.floorwatch.test.rest;

import com.floorwatch.common.utils.DateUtilities;
import java.util.Calendar;

public class TestConvertDateToSecsSince2000 {
    
    public static void main(String[] args) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MONTH, 11);
        date.set(Calendar.DAY_OF_MONTH, 29);
        date.set(Calendar.YEAR, 2015);
        date.set(Calendar.HOUR_OF_DAY, 14);
        date.set(Calendar.MINUTE, 38);
        date.set(Calendar.SECOND, 1);
        date.set(Calendar.MILLISECOND, 0);
        System.out.println("Seconds since 2000: " + DateUtilities.convertMillisSince1970ToSecsSince2000(date.getTimeInMillis()));
    }    
    
}
